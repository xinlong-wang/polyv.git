<?php


namespace polyv\src\channel;

use Illuminate\Support\Facades\Validator;
use polyv\src\InvalidArgumentException;

/**
 * 教师设置
 * Trait TeacherSettings
 * @package polyv\src\channel
 */
trait TeacherSettings
{
    protected $teacher = [];

    /**
     * 讲师昵称
     * @param $nickname
     */
    public function setTeacherNickname($nickname): void
    {
        $this->teacher['nickname'] = $nickname;
    }

    /**
     * 讲师头衔
     * @param $actor
     */
    public function setTeacherActor($actor): void
    {
        $this->teacher['actor'] = $actor;
    }

    /**
     * 讲师密码
     * @param $passwd
     */
    public function setTeacherPasswd($passwd): void
    {
        $this->teacher['passwd'] = $passwd;
    }

    /**
     * 头像图片地址
     * @param $avatar
     */
    public function setTeacherAvatar($avatar): void
    {
        $this->teacher['avatar'] = $avatar;
    }

    public function teacherCheck(): void
    {
        $rules = [
            'nickname' => 'string',
            'actor' => 'string',
            'passwd' => 'alpha_num',
            'avatar' => 'url'
        ];
        $validator = Validator::make($this->teacher, $rules);
        $error = $validator->errors()->first();
        if ($error) {
            throw new InvalidArgumentException($error);
        }
    }
}