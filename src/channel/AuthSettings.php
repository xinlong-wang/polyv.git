<?php

namespace polyv\src\channel;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Validator;
use polyv\src\Basic;
use polyv\src\InvalidArgumentException;

/**
 * 观看条件设置
 * Class AuthSettings
 * @package polyv\src\channel
 */
trait AuthSettings
{
    protected $auth = [];

    /**
     * 主要观看条件为1，次要观看条件为2
     * @param int $rank
     */
    public function setRank(int $rank): void
    {
        $this->auth['rank'] = $rank;
    }

    /**
     * 是否开启条件观看 N：关闭 Y：开启
     * @param string $enabled
     */
    public function setEnabled(string $enabled): void
    {
        $this->auth['enabled'] = $enabled;
    }

    /**
     * pay：付费观看
     * code：验证码观看
     * phone：白名单观看
     * info：登记观看
     * custom：自定义授权观看
     * external：外部授权观看
     * direct：直接授权观看
     * @param string $authType
     */
    public function setAuthType(string $authType): void
    {
        $this->auth['authType'] = $authType;
    }

    /**
     * 当authType为pay时，设置参数，必填。欢迎语标题
     * @param string $payAuthTips
     */
    public function setPayAuthTips(string $payAuthTips): void
    {
        $this->auth['payAuthTips'] = $payAuthTips;
    }

    /**
     * 当authType为pay时，设置参数，必填。价格，单位为元
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->auth['price'] = $price;
    }

    /**
     * 当authType为pay时，设置参数，非必填。付费有效截止日期，格式：yyyy-MM-dd HH:mm
     * @param string $watchEndTime
     */
    public function setWatchEndTime(string $watchEndTime): void
    {
        $this->auth['watchEndTime'] = $watchEndTime;
    }

    /**
     * 当authType为pay时，设置参数，非必填。付费有效时长，单位天。当watchEndTime和validTimePeriod都为空时，表示付费永久有效
     * @param string $validTimePeriod
     */
    public function setValidTimePeriod(string $validTimePeriod): void
    {
        $this->auth['validTimePeriod'] = $validTimePeriod;
    }

    /**
     * 当authType为code时，设置参数，必填。验证码
     * @param string $authCode
     */
    public function setAuthCode(string $authCode): void
    {
        $this->auth['authCode'] = $authCode;
    }

    /**
     * 当authType为code时，设置参数，非必填。提示文案
     * @param string $qcodeTips
     */
    public function setQcodeTips(string $qcodeTips): void
    {
        $this->auth['qcodeTips'] = $qcodeTips;
    }

    /**
     * 当authType为code时，设置参数，非必填。公众号二维码地址
     * @param string $qcodeImg
     */
    public function setQcodeImg(string $qcodeImg): void
    {
        $this->auth['qcodeImg'] = $qcodeImg;
    }

    /**
     * 当authType为phone时，设置参数，非必填。提示文案
     * @param string $authTips
     */
    public function setAuthTips(string $authTips): void
    {
        $this->auth['authTips'] = $authTips;
    }

    /**
     * 当authType为info时，设置参数，必填。登记观看信息，上限为5个
     * -----------------------------------------------------------------------------------------------------------------------------------------
     * |name        |false    |String    |当authType为info时，设置参数，非必填。登记信息名，最多为8字符                                                       |
     * |type        |false    |String    |当authType为info时，设置参数，必填。登记类型 [name：姓名 text：文本 mobile：手机号码 number：数字 option：下拉选项]    |
     * |options        |false    |String    |当authType为info时，设置参数，非必填。下拉选项时，下拉的选项值，以英文逗号分割。选项个数上限为8个；选项内容最多为8字符     |
     * |placeholder    |false    |String    |当authType为info时，设置参数，非必填。文本框输入提示，最多为8字符                                                   |
     * |sms            |false    |String    |当authType为info时，设置参数，非必填。短信验证开关，Y：开启，N：关闭                                                |
     * @param array $infoFields
     */
    public function setInfoFields(array $infoFields): void
    {
        $this->auth['infoFields'] = $infoFields;
    }

    /**
     * 当authType为external时，设置参数，必填。SecretKey
     * @param string $externalKey
     */
    public function setExternalKey(string $externalKey): void
    {
        $this->auth['externalKey'] = $externalKey;
    }

    /**
     * 当authType为external时，设置参数，必填。自定义url
     * @param string $externalUri
     */
    public function setExternalUri(string $externalUri): void
    {
        $this->auth['externalUri'] = $externalUri;
    }

    /**
     * 当authType为custom时，设置参数，必填。SecretKey
     * @param string $externalRedirectUri
     */
    public function setExternalRedirectUri(string $externalRedirectUri): void
    {
        $this->auth['externalRedirectUri'] = $externalRedirectUri;
    }

    /**
     * 当authType为custom时，设置参数，必填。SecretKey
     * @param string $customKey
     */
    public function setCustomKey(string $customKey): void
    {
        $this->auth['customKey'] = $customKey;
    }

    /**
     * 当authType为custom时，设置参数，必填。自定义url
     * @param string $customUri
     */
    public function setCustomUri(string $customUri): void
    {
        $this->auth['customUri'] = $customUri;
    }

    /**
     * 当authType为direct时，设置参数，必填。直接授权SecretKey
     * @param $directKey
     */
    public function setDirectKey($directKey): void
    {
        $this->auth['directKey'] = $directKey;
    }

    public function authCheck(): void
    {
        $rules = [
            'rank' => ['required', 'integer', 'between:1,2'],
            'enabled' => ['required', 'in:Y,N'],
            'authType' => ['string', 'in:pay,code,phone,info,custom,external,direct'],
            'payAuthTips' => ['required_if:authType,pay', 'string'],
            'price' => ['required_if:authType,pay', 'float'],
            'watchEndTime' => ['date'],
            'validTimePeriod' => ['integer'],
            'authCode' => ['required_if:authType,code', 'string'],
            'qcodeTips' => ['string'],
            'qcodeImg' => ['url'],
            'authTips' => ['string'],
            'infoFields' => ['required_if:authType,info', 'array'],
            'infoFields.name' => ['string'],
            'infoFields.type' => ['required_if:authType,info', 'in:name,text,mobile,number,option'],
            'infoFields.options' => ['string'],
            'infoFields.placeholder' => ['string'],
            'infoFields.sms' => ['string', 'in:Y,N'],
        ];

        $validator = Validator::make($this->auth, $rules);
        $error = $validator->errors()->first();
        if ($error) {
            throw new InvalidArgumentException($error);
        }
    }
}
