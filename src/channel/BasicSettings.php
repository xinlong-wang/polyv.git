<?php


namespace polyv\src\channel;


use Illuminate\Support\Facades\Validator;
use polyv\src\InvalidArgumentException;

/**
 * 基础设置
 * "name" => "",// 频道名称
 * "channelPasswd" => "",// 频道密码,长度不能超过16位
 * 'autoPlay' => 1,
 * 'playerColor' => '#666666',
 * 'scene' => 'ppt',
 * 'categoryId' => 0,
 * "maxViewer" => 1000,//最大在线人数
 * "startTime" => 0,// 直播开始时间，13位时间戳，设置为0 表示关闭直播开始时间显示
 * "desc" => "直播介绍",//    直播介绍
 * 'publisher' => '主持人',// 主持人
 * 'linkMicLimit' => -1,//连麦人数，-1：使用账号的连麦人数，范围大于等于-1，小于等于账号的连麦人数，最大16人
 * 'pureRtcEnabled' => 'Y',//是否为无延时直播，默认为N Y：是;N：否
 * 'receive' => 'N',// 是否为接收转播频道，不填或者填其他值为发起转播频道（注：需要开启频道转播功能该参数才生效）Y：表示是 N：表示否
 * 'receiveChannelIds' => '',//接收转播频道号，多个频道号用半角逗号,隔开(注：需要开启频道转播功能该参数才生效)
 * 'onlyOneLiveEnabled' => 'N',// 频道是否只能直播一次，默认为N
 * "coverImg" => "https://my.polyv.net/v_22/assets/dist/images/navbar/logo.png",// 封面图片地址
 * "splashEnabled" => "N",// 引导页开关(Y、N)
 * "splashImg" => "https://my.polyv.net/v_22/assets/dist/images/navbar/logo.png",// 引导图地址
 * "likes" => 666,// 点赞数
 * "pageView" => 1000,//    累积观看数
 * "closeDanmu" => "N",//是否关闭弹幕功能的开关，N表示不关闭，Y表示关闭
 * 'showDanmuInfoEnabled' => '',// 是否显示弹幕信息开关， Y：表示显示 N：表示不显示
 * trait BasicSettings
 * @package polyv\src\channel
 */
trait BasicSettings
{
	protected $basicSetting = [];

	/**
	 * 设置频道名称
	 * @param string $name
	 */
	public function setName(string $name): void
	{
		$this->basicSetting['name'] = $name;
	}

	/**
	 * 设置频道密码
	 * @param string $channelPasswd
	 */
	public function setChannelPasswd(string $channelPasswd): void
	{
		$this->basicSetting['channelPasswd'] = $channelPasswd;
	}

	/**
	 * 是否自动播放 0：不自动播放 1：自动播放，默认1
	 * @param int $autoPlay
	 */
	public function setAutoPlay(int $autoPlay): void
	{
		$this->basicSetting['autoPlay'] = $autoPlay;
	}

	/**
	 * 播放器控制栏颜色，默认：#666666
	 * @param int $playerColor
	 */
	public function setPlayerColor(int $playerColor): void
	{
		$this->basicSetting['playerColor'] = $playerColor;
	}

	/**
	 * 直播场景 alone：活动拍摄 ppt：三分屏 topclass：大班课 seminar：研讨会
	 * @param string $scene
	 */
	public function setScene(string $scene): void
	{
		$this->basicSetting['scene'] = $scene;
	}

	/**
	 * 新建频道的所属分类，如果不提交，则为默认分类（分类ID可通过“查询直播分类”接口得到）
	 * @param int $categoryId
	 */
	public function setCategoryId(int $categoryId): void
	{
		$this->basicSetting['categoryId'] = $categoryId;
	}

	/**
	 * 最大同时在线人数
	 * @param int $maxViewer
	 */
	public function setMaxViewer(int $maxViewer): void
	{
		$this->basicSetting['maxViewer'] = $maxViewer;
	}

	/**
	 * 直播开始时间，11位秒级时间戳
	 * @param int $startTime
	 */
	public function setStartTime(int $startTime): void
	{
		$this->basicSetting['startTime'] = $startTime * 1000;
	}

	/**
	 * 设置直播介绍，可以为html代码
	 * @param string $desc
	 */
	public function setDesc(string $desc): void
	{
		$this->basicSetting['desc'] = $desc;
	}

	/**
	 * 设置主持人
	 * @param string $publisher
	 */
	public function setPublisher(string $publisher): void
	{
		$this->basicSetting['publisher'] = $publisher;
	}

	/**
	 * 连麦人数，-1：使用账号的连麦人数，范围大于等于-1，小于等于账号的连麦人数，最大16人
	 * @param int $linkMicLimit
	 */
	public function setLinkMicLimit(int $linkMicLimit): void
	{
		$this->basicSetting['linkMicLimit'] = $linkMicLimit;
	}

	/**
	 * 是否为无延时直播，默认为N Y：是;N：否
	 * @param $pureRtcEnabled
	 */
	public function setPureRtcEnabled($pureRtcEnabled): void
	{
		$this->basicSetting['pureRtcEnabled'] = $pureRtcEnabled;
	}

	/**
	 * 是否为接收转播频道，不填或者填其他值为发起转播频道（注：需要开启频道转播功能该参数才生效）
	 * Y：表示是
	 * N：表示否
	 * @param string $receive
	 */
	public function setReceive(string $receive): void
	{
		$this->basicSetting['receive'] = $receive;
	}

	/**
	 * 接收转播频道号，多个频道号用半角逗号,隔开(注：需要开启频道转播功能该参数才生效)
	 * @param string $receiveChannelIds
	 */
	public function setReceiveChannelIds(string $receiveChannelIds): void
	{
		$this->basicSetting['receiveChannelIds'] = $receiveChannelIds;
	}

	/**
	 * 频道是否只能直播一次，默认为N Y：是 N：否
	 * @param string $onlyOneLiveEnabled
	 */
	public function setOnlyOneLiveEnabled(string $onlyOneLiveEnabled): void
	{
		$this->basicSetting['onlyOneLiveEnabled'] = $onlyOneLiveEnabled;
	}

	/**
	 * 封面图片地址
	 * @param string $url
	 */
	public function setCoverImg(string $url): void
	{
		$this->basicSetting['coverImg'] = $url;
	}

	/**
	 * 引导页开关(Y、N)
	 * @param string $splashEnabled
	 */
	public function setSplashEnabled(string $splashEnabled): void
	{
		$this->basicSetting['splashEnabled'] = $splashEnabled;
	}

	/**
	 * 引导图地址
	 * @param string $splashImg
	 */
	public function setSplashImg(string $splashImg): void
	{
		$this->basicSetting['splashImg'] = $splashImg;
	}

	/**
	 * 设置点赞人数
	 * @param int $likes
	 */
	public function setLikes(int $likes): void
	{
		$this->basicSetting['likes'] = $likes;
	}

	/**
	 * 设置累积观看数
	 * @param int $pageView
	 */
	public function setPageView(int $pageView): void
	{
		$this->basicSetting['pageView'] = $pageView;
	}

	/**
	 * 是否关闭弹幕功能的开关，N表示不关闭，Y表示关闭
	 * @param string $closeDanmu
	 */
	public function setCloseDanmu(string $closeDanmu): void
	{
		$this->basicSetting['closeDanmu'] = $closeDanmu;
	}

	/**
	 * 是否显示弹幕信息开关， Y：表示显示 N：表示不显示
	 * @param string $showDanmuInfoEnabled
	 */
	public function setShowDanmuInfoEnabled(string $showDanmuInfoEnabled): void
	{
		$this->basicSetting['showDanmuInfoEnabled'] = $showDanmuInfoEnabled;
	}

	public function basicCheck(): void
	{
		$switch = 'in:Y,N';
		$rules = [
			'name' => ['string'],
			'channelPasswd' => ['alpha_num'],
			'autoPlay' => ['integer', 'between:0,1'],
			'playerColor' => ['string'],
			'scene' => ['in:alone,ppt,topclass,seminar'],
			'categoryId' => ['integer'],
			'maxViewer' => ['integer'],
			'startTime' => ['integer'],
			'desc' => ['string'],
			'publisher' => ['string'],
			'linkMicLimit' => ['between:-1,16'],
			'pureRtcEnabled' => [$switch],
			'receive' => ['in:Y,N'],
			'receiveChannelIds' => ['string'],
			'onlyOneLiveEnabled' => [$switch],
			'coverImg' => ['url'],
			'splashEnabled' => [$switch],
			'splashImg' => ['url'],
			'likes' => ['integer'],
			'pageView' => ['integer'],
			'closeDanmu' => [$switch]
		];
		$validator = Validator::make($this->basicSetting, $rules);
		$errors = $validator->errors()->first();
		if ($errors) {
			throw new InvalidArgumentException($errors);
		}
	}
}
