<?php

namespace polyv\src\channel;

use GuzzleHttp\Client;

class PasswdSetting
{
    protected $url = "https://api.polyv.net/live/v2/channels/%s/passwdSetting";

    public function send()
    {
        $client = new Client();
        $client->post(sprintf($this->url, env('POLYV_USER_ID')), [
            'http_errors' => false,
            'json' => [
            ]
        ]);
    }
}