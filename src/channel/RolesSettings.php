<?php


namespace polyv\src\channel;

use Illuminate\Support\Facades\Validator;
use polyv\src\InvalidArgumentException;

/**
 * 角色设置
 * Trait RolesSettings
 * @package polyv\src\channel
 */
trait RolesSettings
{
    protected $role = [];

    /**
     * 昵称
     * @param string $nickname
     */
    public function setRolesNickname(string $nickname): void
    {
        $this->role[0]['nickname'] = $nickname;
    }

    /**
     * 头衔
     * @param string $actor
     */
    public function setRolesActor(string $actor): void
    {
        $this->role[0]['actor'] = $actor;
    }

    /**
     * 密码
     * @param string $passwd
     */
    public function setRolesPasswd(string $passwd): void
    {
        $this->role[0]['passwd'] = $passwd;
    }

    /**
     * 头像图片地址
     * @param string $avatar
     */
    public function setRolesAvatar(string $avatar): void
    {
        $this->role[0]['avatar'] = $avatar;
    }

    /**
     * 角色类型 Assistant-助教 Guest-嘉宾
     * @param string $role
     */
    public function setRole(string $role): void
    {
        $this->role[0]['role'] = $role;
    }

    /**
     * 批量添加角色和单独设置选其一
     * $role = [
     *          ['nickname' => '',
     *              'actor' => '',
     *              'passwd' => '',
     *              'avatar' => '',
     *              'role' => 'Assistant']
     * ];
     * @param array $roles
     */
    public function batchRoles(array $roles): void
    {
        $this->role = $roles;
    }

    public function rolesCheck(): void
    {
        $rules = [
            '*.nickname' => ['string'],
            '*.actor' => ['string'],
            '*.password' => ['string'],
            '*.avatar' => ['url'],
            '*.role' => ['string', 'in:Assistant,Guest']
        ];
        $validator = Validator::make($this->role, $rules);
        $error = $validator->errors()->first();
        if ($error) {
            throw new InvalidArgumentException($error);
        }
    }
}