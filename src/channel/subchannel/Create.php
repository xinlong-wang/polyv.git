<?php


namespace polyv\src\channel\subchannel;


use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Log;

/**
 * 创建单个子频道
 * Class Create
 * @package polyv\src\channel\subchannel
 */
class Create extends SubChannel
{

    /**
     * @param string $actor
     */
    public function setActor(string $actor): void
    {
        $this->params['actor'] = $actor;
    }

    /**
     * @param string $avatar
     */
    public function setAvatar(string $avatar): void
    {
        $this->params['avatar'] = $avatar;
    }

    /**
     * @param string $nickname
     */
    public function setNickname(string $nickname): void
    {
        $this->params['nickname'] = $nickname;
    }

    /**
     * @param string $role
     */
    public function setRole(string $role): void
    {
        $this->params['role'] = $role;
    }

    public function send()
    {
        parent::send();
        $url = 'https://api.polyv.net/live/v2/channelAccount/%s/add?';
        if (empty($this->channelId)) {
            throw  new \InvalidArgumentException('频道ID不能为空');
        }
        $request = new Request('POST', sprintf($url, $this->channelId) . http_build_query($this->params));
        $client = new Client();
        $content = $client->send($request, ['http_errors' => false])->getBody()->getContents();
        Log::debug('创建子频道：' . $content);
        return $content;
    }
}