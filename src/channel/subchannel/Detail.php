<?php


namespace polyv\src\channel\subchannel;


use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Log;

/**
 * 查询单个子频道信息
 * Class Detail
 * @package polyv\src\channel\subchannel
 */
class Detail extends SubChannel
{

    /**
     * 子频道号（不能以数字类型提交，否则可能去掉子频道号前的00）
     * @param string $account
     */
    public function setAccount(string $account): void
    {
        $this->params['account'] = $account;
    }

    public function send()
    {
        parent::send();
        if (empty($this->channelId)) {
            throw new \InvalidArgumentException('频道ID不能为空');
        }
        if (empty($this->params['account'])) {
            throw new \InvalidArgumentException('子频道号不能为空');
        }
        $url = 'https://api.polyv.net/live/v2/channelAccount/%s/account?';
        $request = new Request('GET', sprintf($url, $this->channelId) . http_build_query($this->params));
        $client = new Client();
        $content = $client->send($request, ['http_errors' => false])->getBody()->getContents();
        Log::debug('修改子频道：' . $content);
        return $content;
    }
}