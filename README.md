## 保利威直播

### 直播频道创建

```php
use polyv\src\channel\Create;

$di = new Create();
// 基础设置
$di->setName('test');
$di->setChannelPasswd('polyv123');
$di->setAutoPlay(1);
$di->setMaxViewer(1000);
$di->setScene('seminar');
$di->setPureRtcEnabled('Y');

// 观看条件设置
$di->setRank(1);
$di->setEnabled('Y');
$di->setAuthType('external');
$di->setExternalKey(env('POLYV_SECRET_KEY'));
$di->setExternalUri(env('APP_URL') . '/api/home/liveRoom/userinfo');

// 回放设置
$di->setGlobalSettingEnabled('Y');
$di->setplaybackEnabled('Y');
// 角色单个设置
$di->setRolesNickname('name');
$di->setRolesActor('头衔');
$di->setRolesPasswd('passwd');
$di->setRolesAvatar('url');
$di->setRole('Assistant');
// 角色批量设置
    $di->batchRoles([
    ['nickname' => 'name', 'actor' => '1234', 'passwd' => '1234', 'avatar' => 'url']
    ]);

$res = $di->send();
```
### 频道菜单设置

```php
use polyv\src\channel\Menu;

$menu = new Menu();
$menu->setName($this->request->input('name'));
$menu->setChannelId($this->request->input('channel_id'));
$menu->setContent($this->request->input('content'));
$menu->setType('iframe');
$ret = $menu->send();
```

### 统计直播间内多场次的直播和观看数据

```php
use polyv\src\channel\statistics\SessionStats;

$obj = new SessionStats();
$obj->setChannelId('2270342');
$obj->setQueryTime(mktime(0, 0, 0, 4, 29, 2021), mktime(23, 0, 0, 4, 29, 2021) );
$obj->send();
```

### 查询频道观看日志

```php
use polyv\src\channel\statistics\ViewLog;

$obj = new ViewLog();
$obj->setChannelId('2270342');
$obj->setQueryTime(mktime(10, 0, 0, 4, 29, 2021), mktime(16, 0, 0, 4, 29, 2021));
$obj->setPage('1');
$obj->send();
```

### 查询历史聊天信息

```php
use polyv\src\channel\chat\History;

$obj = new History();
$obj->setChannelId(2270342);
$obj->setQueryTime(mktime(10, 0, 0, 4, 29, 2021), mktime(16, 0, 0, 4, 30, 2021));
$obj->send();
```